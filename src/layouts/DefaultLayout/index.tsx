import { Outlet } from "react-router-dom";
import { Box, Container } from "@mui/material";
import { FC } from "react";
import { Header } from "../../components/Header";

export const DefaultLayout: FC = () => {
  return (
    <Box
      sx={{
        bgcolor: "#f4f6f9",
      }}
    >
      <Header />
      <Container
        sx={{
          padding: 0,
          paddingY: 3,
          height: "100%",
          margin: "0 auto",
        }}
      >
        <Outlet />
      </Container>
    </Box>
  );
};
