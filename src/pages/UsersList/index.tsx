import { Box, CircularProgress, useTheme } from "@mui/material";
import { DataTable } from "../../components/Table";
import useUserList from "./useUsersList";
import { useAppSelector } from "../../store/hooks";
import { IUserDetails } from "../../types/users";


export const UsersList = () => {
  const { columnConfigData, handleRowClick } = useUserList();
  const { data: users, status } = useAppSelector(
    (state) => state.userListSlice
  );

  return (
    <>
      <Box>
        {status === "loading" ? (
          <Box sx={{ textAlign: "center" }}>
            <CircularProgress />
          </Box>
        ) : (
        (  !!users &&
          users?.length) ? (
            <DataTable<IUserDetails>
              data={users}
              columnConfig={columnConfigData}
              onRowClick={handleRowClick}
            />
          ) : "No Data"
        )}
      </Box>
    </>
  );
};
