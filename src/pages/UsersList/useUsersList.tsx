import { useEffect } from "react";
import { IColumnConfig } from "../../components/Table/types";
import { useAppDispatch } from "../../store/hooks";
import { getUserListTC, setSingleData } from "../../store/reducers/user-list/slice";
import { IUserDetails } from "../../types/users";
import { useNavigate } from "react-router-dom";

const useUsersList = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(getUserListTC());
  }, []);

  const handleRowClick = (e:Event,rowData: IUserDetails): void => {
    dispatch(setSingleData(rowData))
    navigate(`/user-profile/${rowData.id}`);
  };

  const columnConfigData: Array<IColumnConfig<IUserDetails>> = [
    {
      title: "Employment",
      setRow: (row: IUserDetails) => <p>{row.employment.title}</p>,
      isVisible: true,
      customStyle: { minWidth: "80px" },
    },
    {
      title: "Name Surname",
      setRow: (row: IUserDetails) => (
        <p>{row.first_name + " " + row.last_name}</p>
      ),
      isVisible: true,
      customStyle: { minWidth: "80px" },
    },
    {
      title: "Username",
      setRow: (row: IUserDetails) => <p>{row.username}</p>,
      isVisible: true,
      customStyle: { minWidth: "80px" },
    },
    {
      title: "Email",
      setRow: (row: IUserDetails) => <p>{row.email}</p>,
      isVisible: true,
      customStyle: { minWidth: "80px" },
    },
    {
      title: "Phone Number",
      setRow: (row: IUserDetails) => <p>{row.phone_number}</p>,
      isVisible: true,
      customStyle: { minWidth: "80px" },
    },
    {
      title: "Address",
      setRow: (row: IUserDetails) => (
        <p>
          {row.address.city +
            "," +
            row.address.street_name +
            " " +
            row.address.street_address}
        </p>
      ),
      isVisible: true,
      customStyle: { minWidth: "80px" },
    },
  ];

  return {
    columnConfigData,
    handleRowClick,
  };
};
export default useUsersList;
