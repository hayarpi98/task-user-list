import { useEffect } from "react";
import { useAppSelector } from "../../store/hooks";
import { useFormik } from "formik";
import { IInitialValues } from "../../types/users";
import { useNavigate } from "react-router-dom";

const useUserProfile = () => {
  const { singleUser: singleUser } = useAppSelector((state) => state.userListSlice);
  const navigate = useNavigate()
  const initialValues: IInitialValues = {
    first_name: "",
    last_name: "",
    username: "",
    gender: "",
    date_of_birth: "",
    phone_number: "",
    social_insurance_number: "",
    country: "",
    city: "",
    state: "",
    zip_code: "",
    street_address: "",
    street_name: "",
    email: "",
    password: "",
    card_number: "",
    subscription_plan:"",
    subscription_status:"",
    subscription_payment_method:"",
    term:""
  };
  const formik = useFormik({
    initialValues,
    onSubmit: (values) => {
      if (values) {
        console.log("SuccessResult:", values);
      }
    },
  });
  useEffect(() => {
    if(singleUser !== null) {
        formik.setFieldValue('first_name', singleUser.first_name);
        formik.setFieldValue('last_name', singleUser.last_name);
        formik.setFieldValue('username', singleUser.username);
        formik.setFieldValue('gender', singleUser.gender);
        formik.setFieldValue('date_of_birth', singleUser.date_of_birth);
        formik.setFieldValue('phone_number', singleUser.phone_number);
        formik.setFieldValue('social_insurance_number', singleUser.social_insurance_number);
        formik.setFieldValue('country', singleUser.address.country);
        formik.setFieldValue('city', singleUser.address.city);
        formik.setFieldValue('state', singleUser.address.state);
        formik.setFieldValue('zip_code', singleUser.address.zip_code);
        formik.setFieldValue('street_address', singleUser.address.street_address);
        formik.setFieldValue('street_name', singleUser.address.street_name);
        formik.setFieldValue('email', singleUser.email);
        formik.setFieldValue('password', singleUser.password);
        formik.setFieldValue('card_number', singleUser.credit_card.cc_number);
        formik.setFieldValue('subscription_plan', singleUser.subscription.plan);
        formik.setFieldValue('subscription_status', singleUser.subscription.status);
        formik.setFieldValue('subscription_payment_method', singleUser.subscription.payment_method);
        formik.setFieldValue('term', singleUser.subscription.term);
    } else {
      navigate('/')
    }
  }, [singleUser]);
  return {formik};

};
export default useUserProfile;
