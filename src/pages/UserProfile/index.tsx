import useUserProfile from "./useUserProfile";
import { Box, Button, Grid } from "@mui/material";
import { Input } from "../../components/CustomInput";
import { DatePicker } from "@mui/x-date-pickers";
import dayjs, { Dayjs } from "dayjs";
import { ColorModeContext } from "../../App";
import { useContext } from "react";

export const UserProfile = () => {
  const { formik } = useUserProfile();
  const { mode } = useContext(ColorModeContext);

  return (
    <>
      <Box sx={{ display: { md: "flex", xs: "block" }, justifyContent: "space-between" }}>
        <Grid sx={{ marginBottom: "20px" }}>
          <Input
            InputLabelProps={{
              shrink: true,
            }}
            onChange={formik.handleChange("first_name")}
            value={formik.values.first_name}
            label={"Name"}
          />
        </Grid>
        <Grid sx={{ marginBottom: "20px" }}>
          <Input
            InputLabelProps={{
              shrink: true,
            }}
            onChange={formik.handleChange("last_name")}
            value={formik.values.last_name}
            label={"Surname"}
          />
        </Grid>
        <Grid>
          <Input
            InputLabelProps={{
              shrink: true,
            }}
            onChange={formik.handleChange("username")}
            value={formik.values.username}
            label={"Username"}
          />
        </Grid>
        <Grid>
          <Input
            InputLabelProps={{
              shrink: true,
            }}
            onChange={formik.handleChange("gender")}
            value={formik.values.gender}
            label={"Gender"}
          />
        </Grid>
      </Box>
      <Box
        sx={{
          display: { md: "flex", xs: "block" },
          justifyContent: "space-between",
          py: 3,
          px: 0,
          width: { md: "73%", xs: "100%" }
        }}
      >
        <Grid sx={{
          marginBottom: "20px", width: { md: "23%", xs: "100%" }
        }}>
          <DatePicker
            sx={{
              width: '100%',
              '.MuiInputBase-root': {
                height: '45px',
                width: '100%'
              },

              label: {
                fontSize: '11px',
              },
            }}
            value={dayjs(formik.values.date_of_birth)}
            onChange={(date: dayjs.Dayjs | null | string) => {
              if (date !== null) {
                const dateString =
                  typeof date === "string" ? date : date.format();
                const inputDate = new Date(dateString);
                const formattedDate = inputDate.toISOString().split("T")[0];
                formik.setFieldValue("date_of_birth", formattedDate);
              }
            }}
          />
        </Grid>
        <Grid sx={{ marginBottom: "20px" }}>
          <Input
            InputLabelProps={{
              shrink: true,
            }}
            onChange={formik.handleChange("phone_number")}
            value={formik.values.phone_number}
            label={"Phone Number"}
          />
        </Grid>
        <Grid sx={{ marginBottom: "20px" }}>
          <Input
            InputLabelProps={{
              shrink: true,
            }}
            onChange={formik.handleChange("social_insurance_number")}
            value={formik.values.social_insurance_number}
            label={"Social insurance number"}
          />
        </Grid>
      </Box>
      <Box sx={{ display: { md: "flex", xs: "block" }, justifyContent: "space-between" }}>
        <Grid sx={{ marginBottom: "20px" }}>
          <Input
            InputLabelProps={{
              shrink: true,
            }}
            onChange={formik.handleChange("country")}
            value={formik.values.country}
            label={"Country"}
          />
        </Grid>
        <Grid sx={{ marginBottom: "20px" }}>
          <Input
            InputLabelProps={{
              shrink: true,
            }}
            onChange={formik.handleChange("city")}
            value={formik.values.city}
            label={"City"}
          />
        </Grid>
        <Grid sx={{ marginBottom: "20px" }}>
          <Input
            InputLabelProps={{
              shrink: true,
            }}
            onChange={formik.handleChange("state")}
            value={formik.values.state}
            label={"State"}
          />
        </Grid>
        <Grid sx={{ marginBottom: "20px" }}>
          <Input
            InputLabelProps={{
              shrink: true,
            }}
            onChange={formik.handleChange("zip_code")}
            value={formik.values.zip_code}
            label={"Zip Code"}
          />
        </Grid>
      </Box>
      <Box
        sx={{
          display: { md: "flex", xs: "block" },
          justifyContent: "space-between",
          py: 3,
          px: 0,
          width: { md: "45%", xs: "100%" },
        }}
      >
        <Grid sx={{ marginBottom: "20px" }}>
          <Input
            InputLabelProps={{
              shrink: true,
            }}
            onChange={formik.handleChange("street_address")}
            value={formik.values.street_address}
            label={"Street Address"}
          />
        </Grid>
        <Grid sx={{ marginBottom: "20px" }}>
          <Input
            InputLabelProps={{
              shrink: true,
            }}
            onChange={formik.handleChange("street_name")}
            value={formik.values.street_name}
            label={"Street Name"}
          />
        </Grid>
      </Box>
      <Box
        sx={{
          display: { md: "flex", xs: "block" },
          alignItems: "flex-start",
          justifyContent: "space-between",
          px: 0,
        }}
      >
        <Grid
          sx={{
            width: { md: "17%", xs: "100%" }
          }}
        >
          <Box sx={{  marginBottom: {xs:"20px",md:"10px" }}}>
            Status
            <h3>{formik.values.subscription_status}</h3>
          </Box>

          <Box sx={{  marginBottom: {xs:"20px",md:"10px" }}}>
            Payment Method
            <h3>{formik.values.subscription_payment_method}</h3>
          </Box>

          <Box sx={{  marginBottom: {xs:"20px",md:"10px" }}}>
            Term
            <h3>{formik.values.term}</h3>
          </Box>
        </Grid>
        <Box>
          <Grid sx={{ marginBottom: "20px" }}>
            <Input
              InputLabelProps={{
                shrink: true,
              }}
              InputProps={{
                inputProps: {
                  maxLength: 19,
                },
              }}
              onChange={formik.handleChange("card_number")}
              value={formik.values.card_number}
              label={"Card Number"}
            />
          </Grid>
          <Grid sx={{ marginBottom: "20px" }}>
            <Input
              InputLabelProps={{
                shrink: true,
              }}
              onChange={formik.handleChange("subscription_plan")}
              value={formik.values.subscription_plan}
              label={"Subscription Plan"}
            />
          </Grid>
        </Box>
        <Box sx={{
          px: 0, width: { md: "45%", xs: "100%" },
        }}>
          <Grid>
            <Input
              InputLabelProps={{
                shrink: true,
              }}
              onBlur={formik.handleBlur("email")}
              onChange={formik.handleChange("email")}
              value={formik.values.email}
              label={"Email"}
            />
          </Grid>
          <Grid sx={{ marginBottom: "20px" }}>
            <Input
              InputLabelProps={{
                shrink: true,
              }}
              type="password"
              onChange={formik.handleChange("password")}
              value={formik.values.password}
              style={{ marginTop: "20px" }}
              label={"Password"}
            />
          </Grid>
        </Box>
      </Box>
      <Box sx={{ textAlign: "end" }}>
        <Button onClick={() => formik.handleSubmit()} variant="contained" sx={{ color: mode === 'dark' ? 'white' : 'black', background: mode === 'dark' ? 'black' : 'white' }}>
          Submit
        </Button>
      </Box>
    </>
  );
};
