import { createBrowserRouter } from 'react-router-dom';
import ROUTE from "./paths";
import { UserProfile, UsersList } from "../pages";
import { DefaultLayout } from '../layouts/DefaultLayout';

const router = createBrowserRouter([
    {
        element: <DefaultLayout />,
        children: [
            {
                path: ROUTE.HOME,
                element: <UsersList />
            },
            {
                path: ROUTE.USER_PROFILE,
                element: <UserProfile />
            },
        ]
    },
]);


export default router