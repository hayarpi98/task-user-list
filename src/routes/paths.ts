const ROUTE = {
    HOME: '/',
    USER_PROFILE:'/user-profile/:id'
};
export default ROUTE;