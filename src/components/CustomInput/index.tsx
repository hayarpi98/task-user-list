"use client";

import {
  IconButton,
  InputAdornment,
  TextField as TextFieldMaterial,
  TextFieldProps,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import { FC, useState } from "react";

export const TextField = styled(TextFieldMaterial)(() => ({
  "& label.MuiInputLabel-root": {
    color: "gray",
  },
  "& .MuiFormHelperText-root": {
    color: "#d32f2f",
    marginLeft: 0,
    fontSize: 12,
  },

  "& input": {
    color: "black",
  },
  "& .MuiInput-underline:after": {
    borderBottomColor: "gray",
  },
  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      borderColor: "darkgray",
    },
    "&:hover fieldset": {},
    "&.Mui-focused fieldset": {
      borderColor: "rgb(47, 47, 165)",
    },
  },
}));
const Radius = {
  style: {
    height: 45,
  },
};

export const Input: FC<TextFieldProps> = (props) => {
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  if (props.type === "password") {
    return (
      <TextField
        {...props}
        size={props.size || "small"}
        type={showPassword ? "text" : "password"}
        InputLabelProps={{
          shrink: true,
        }}
        helperText={props.helperText}
        error={props.error}
        InputProps={{
          ...props.InputProps,
          ...Radius,
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                sx={{ fontSize: "13px" }}
              >
                {showPassword ? "Hide" : "Show"}
              </IconButton>
            </InputAdornment>
          ),
        }}
        sx={{width:"100% "}}
        />
    );
  }
  return (
    <TextField
      helperText={props.helperText}
      error={props.error}
      size={props.size || "small"}
      {...props}
      InputLabelProps={{
        shrink: true,
      }}
      sx={{width:"100%"}}
      InputProps={{ ...Radius, ...props.InputProps }}
    />
  );
};
