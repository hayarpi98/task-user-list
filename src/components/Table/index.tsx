import React, { useContext, useState } from "react";
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from "@mui/material";

import { IColumnConfig, type IProps } from "./types";
import { ColorModeContext } from "../../App";

export function DataTable<T>({
  data,
  columnConfig,
  onRowClick,
}: IProps<T>): JSX.Element {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const visibleData = data.slice(
    page * rowsPerPage,
    page * rowsPerPage + rowsPerPage
  );
  const { mode } = useContext(ColorModeContext);

  return (
    <TableContainer>
      <Table sx={{ padding: 2 }}>
        <TableHead>
          <TableRow>
            {columnConfig.map((column: IColumnConfig<T>, index) => {
              return (
                column.isVisible && (
                  <TableCell
                    key={index}
                    sx={{
                      color: mode === "dark" ? "white" : "black",
                      background: mode === "dark" ? "#696969" : "#ebebeb",
                    }}
                  >
                    <p style={{ fontWeight: "700" }}>{column.title}</p>
                  </TableCell>
                )
              );
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {visibleData.map((item, index) => (
            <TableRow
              key={index}
              onClick={(e) => onRowClick && onRowClick(e, item)}
              sx={{
                cursor: "pointer",
              }}
            >
              {columnConfig.map((column: IColumnConfig<T>, i) => {
                return (
                  <TableCell
                    key={column.id ? column.id : i}
                    sx={{
                      borderColor: mode === "dark" ? "white" : "#696969",
                      color: "black",
                      background: mode === "dark" ? "#c0c0c0" : "white",
                    }}
                  >
                    {column.setRow(item, index)}
                  </TableCell>
                );
              })}
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        sx={{
          borderColor: mode === "dark" ? "white" : "#696969",
          color: "black",
        }}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </TableContainer>
  );
}
