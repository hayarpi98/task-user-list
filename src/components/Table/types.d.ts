import { ActionsTypeEnum } from '@/enum';
interface IColumnConfig<T> {
	title: string;
	setRow: (data: T, index?: number) => React.ReactNode;
	isVisible: boolean;
	customStyle: React.CSSProperties;
	id?: number;
}
interface IActions {
	type: ActionsTypeEnum;
	style?: React.CSSProperties;
}

interface IProps<T> {
	data: T[];
	columnConfig: Array<IColumnConfig<T>>;
	children?: ReactNode;
	onRowClick?: (event, id: T) => void,
	currentPage?: number;
	totalItems?: number;
	itemsPerPage?: number;
	onPageChange?: (page: number) => void;
}

export type { IActions, IColumnConfig, IProps };