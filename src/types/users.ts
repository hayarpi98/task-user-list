export type IUser = IUserDetails[];

export interface IUserDetails {
  id: number;
  uid: string;
  password: string;
  first_name: string;
  last_name: string;
  username: string;
  email: string;
  avatar: string;
  gender: string;
  phone_number: string;
  social_insurance_number: string;
  date_of_birth: string;
  employment: IEmployment;
  address: IAddress;
  credit_card: ICreditCard;
  subscription: ISubscription;
}

export interface IEmployment {
  title: string;
  key_skill: string;
}

export interface IAddress {
  city: string;
  street_name: string;
  street_address: string;
  zip_code: string;
  state: string;
  country: string;
  coordinates: ICoordinates;
}

export interface ICoordinates {
  lat: number;
  lng: number;
}

export interface ICreditCard {
  cc_number: string;
}

export interface ISubscription {
  plan: string;
  status: string;
  payment_method: string;
  term: string;
}

export interface IInitialValues {
  first_name: string;
  last_name: string;
  username: string;
  gender: string;
  date_of_birth: string;
  phone_number: string;
  social_insurance_number: string;
  country: string;
  city: string;
  state: string;
  zip_code: string;
  street_address: string;
  street_name: string;
  email: string;
  password: string;
  card_number: string;
  subscription_plan: string;
  subscription_status:string;
  subscription_payment_method:string;
  term:string
}
