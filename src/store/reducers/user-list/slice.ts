import {
  createAsyncThunk,
  createSlice,
} from "@reduxjs/toolkit";
import { getUserListAPI } from "./api";
import { IUser, IUserDetails } from "../../../types/users";
import axios from "axios";

interface IState {
  data: IUser | null;
  singleUser:IUserDetails | null;
  status: "idle" | "loading" | "failed";
}

const initialState: IState = {
  data: null,
  singleUser:null,
  status: "idle",
};

export const getUserListTC = createAsyncThunk(
  "userList",
  async () => {
    try {
      const response = await getUserListAPI();
      return response.data
    } catch (err) {
      if (axios.isAxiosError(err)) {
        const error = err;
        if (!error.response) {
          throw err;
        }
      }
      throw err;
    }
  }
);

export const userListSlice = createSlice({
  name: "userList",
  initialState,
  reducers: {
    setSingleData: (state, action) => {
			state.singleUser = action.payload;
		},
  },
  extraReducers: (builder) => {
    builder
      .addCase(getUserListTC.pending, (state) => {
        state.status = "loading";
      })
      .addCase(getUserListTC.fulfilled, (state:IState, action) => {
        state.data = action.payload
        state.status = "idle";
      })
      .addCase(getUserListTC.rejected, (state) => {
        state.status = "failed";
      });
  },
});
export const { setSingleData } = userListSlice.actions;

export default userListSlice.reducer;
