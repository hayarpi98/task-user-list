import axios from "axios";
import { IUser } from "../../../types/users";

export const getUserListAPI = async () => {
    return await axios.get<IUser>('https://random-data-api.com/api/v2/users?size=100')
}

