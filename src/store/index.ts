import {
  configureStore,
  combineReducers,
  AnyAction,
  Reducer,
  ThunkAction,
  Action,
} from "@reduxjs/toolkit";

import {
  userListSlice
} from "./reducers";

const appReducer = combineReducers({
  userListSlice
});

const rootReducer: Reducer = (state: RootState, action: AnyAction) => {
  return appReducer(state, action);
};

export const store = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<typeof appReducer>;

export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
