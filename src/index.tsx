import { StrictMode } from "react";
import App from "./App";
import theme from "./theme";
import { ThemeProvider } from "@mui/material/styles";
import "./index.scss";
import { createRoot } from "react-dom/client";
import { store } from "./store";
import { Provider as ReduxProvider } from "react-redux";

const rootElement: HTMLElement | null =
document.getElementById("root") ?? document.createElement("div");
const root = createRoot(rootElement);
root.render(
  <StrictMode>
    <ThemeProvider theme={theme}>
      <ReduxProvider store={store}>
        <App />
      </ReduxProvider>
    </ThemeProvider>
  </StrictMode>
);
